In order to learn and test React, you should set up a React Environment on your computer.

This tutorial uses the create-react-app.

The create-react-app is an officially supported way to create React applications.

If you have NPM and Node.js installed, you can create a React application by first installing the create-react-app.

Install create-react-app by running this command in your terminal:

    C:\Users\Your Name>npm install -g create-react-app
You are now ready to create your first React application!

Run this command to create a React application named myfirstreact:

    C:\Users\Your Name>npx create-react-app myfirstreact
    
The create-react-app will set up everything you need to run a React application.